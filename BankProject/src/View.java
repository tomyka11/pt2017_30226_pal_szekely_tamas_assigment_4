import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class View extends JFrame {
	private JTextField pers_name=new JTextField(15);
	private JTextField pers_age=new JTextField(15);
	private JTextField pers_id=new JTextField(15);
	
	private JTextField acc_id=new JTextField(15);
	private JTextField accp_name=new JTextField(15);
    private JTextField acc_bal=new JTextField(15);
    
    private JTextField accid=new JTextField(15);
    private JTextField money=new JTextField(15);
    
    private JButton add_pers=new JButton("ADD");
	private JButton edit_pers=new JButton("EDIT");
	private JButton delete_pers=new JButton("DELETE_PERS");
	private JButton find_pers=new JButton("FIND");
	
	private JButton add_acc=new JButton("ADD SPENDING");
	private JButton add_acc1=new JButton("ADD SAVING");
	private JButton edit_acc=new JButton("EDIT");
	private JButton delete_acc=new JButton("DELETE");
	private JButton find_acc=new JButton("FIND");
	
	private JButton add_money=new JButton("ADD");
	private JButton sub_money=new JButton("WITHDRW");
	public View(){
		JPanel p=new JPanel();
		p.setLayout(new BoxLayout(p,BoxLayout.Y_AXIS));
		
		JPanel pan1=new JPanel();
		pan1.setLayout(new FlowLayout());
		
		JPanel pan2=new JPanel();
		pan2.setLayout(new BoxLayout(pan2,BoxLayout.X_AXIS));
		
		JPanel pan3=new JPanel();
		pan3.setLayout(new FlowLayout());
		
		JPanel pan4=new JPanel();
		pan4.setLayout(new FlowLayout());
		
		
		
		JPanel pan7=new JPanel();
		pan7.setLayout(new BoxLayout(pan7,BoxLayout.X_AXIS));
		
		JPanel pan8=new JPanel();
		pan8.setLayout(new FlowLayout());
		
		JPanel pan9=new JPanel();
		pan9.setLayout(new FlowLayout());
		
		JPanel pan10=new JPanel();
		pan10.setLayout(new FlowLayout());
		
		JPanel pan11=new JPanel();
		pan11.setLayout(new BoxLayout(pan11,BoxLayout.X_AXIS));
		
		JPanel pan12=new JPanel();
		pan12.setLayout(new FlowLayout());
		
		JPanel pan13=new JPanel();
		pan13.setLayout(new FlowLayout());
		
		JPanel pan14=new JPanel();
		pan14.setLayout(new FlowLayout());
		
		
		
		
		p.add(pan1);
		p.add(pan2);
		p.add(pan3);
        p.add(pan4);
        
		p.add(pan7);
		p.add(pan8);
		p.add(pan9);
		p.add(pan10);
		
		p.add(pan11);
		p.add(pan12);
		p.add(pan13);
		p.add(pan14);
		
		pan1.add(new JLabel("Person Operations"));
		pan1.setBackground(Color.GREEN);
		
		pan2.add(new JLabel("Name"));
		pan2.add(Box.createRigidArea(new Dimension(150,0)));
		pan2.add(new JLabel("Age"));
		pan2.add(Box.createRigidArea(new Dimension(160,0)));
		pan2.add(new JLabel("Id"));
		pan2.add(Box.createRigidArea(new Dimension(170,0)));
		
		pan3.add(pers_name);
		pan3.add(pers_age);
		pan3.add(pers_id);
		
		pan4.add(add_pers);
		pan4.add(edit_pers);
		pan4.add(delete_pers);
		pan4.add(find_pers);
		
		pan7.add(new JLabel("Account Operations"));
		pan7.setBackground(Color.GRAY);
		
		pan8.add(Box.createRigidArea(new Dimension(150,0)));
		pan8.add(new JLabel("Id"));
		pan8.add(Box.createRigidArea(new Dimension(170,0)));
		pan8.add(new JLabel("Name"));
		pan8.add(Box.createRigidArea(new Dimension(140,0)));
		pan8.add(new JLabel("Balance"));
		pan8.add(Box.createRigidArea(new Dimension(160,0)));
		
		pan9.add(acc_id);
		pan9.add(accp_name);
		pan9.add(acc_bal);
		
		pan10.add(add_acc);
		pan10.add(add_acc1);
		pan10.add(edit_acc);
		pan10.add(delete_acc);
		pan10.add(find_acc);
		
		pan11.add(new JLabel("Operations with money"));
		pan11.setBackground(Color.GREEN);
		
		pan12.add(Box.createRigidArea(new Dimension(150,0)));
		pan12.add(new JLabel("Account id"));
		pan12.add(Box.createRigidArea(new Dimension(170,0)));
		pan12.add(new JLabel("Money"));
		pan12.add(Box.createRigidArea(new Dimension(140,0)));
		
		pan13.add(accid);
		pan13.add(money);
		
		pan14.add(add_money);
		pan14.add(sub_money);
		
		
		
		
		
		this.setContentPane(p);
	    this.pack();
	    this.setTitle("BankManagement");
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
		
		
	}
	
	public String getPersID() {
        return pers_id.getText();
    }
	
	public String getPersName(){
		return pers_name.getText();
	}
	
	public String getPersAge(){
		return pers_age.getText();
	}
	
	public String getAccBalance(){
		return acc_bal.getText();
	}
	
	public String getAccID(){
		return acc_id.getText();
	}
	
	public String getAccName(){
		return accp_name.getText();
	}
	
	public String getAccId2(){
		return accid.getText();
	}
	public String getmoney(){
		return money.getText();
	}
	
	public void addAddBankPersListener(ActionListener mal) {
        add_pers.addActionListener(mal);
    }
	
	public void addDeleteBankPersListener(ActionListener mal) {
        delete_pers.addActionListener(mal);
    }
	
	public void addEditBankPersListener(ActionListener mal) {
        edit_pers.addActionListener(mal);
    }
	
	public void addFindBankPersListener(ActionListener mal) {
        find_pers.addActionListener(mal);
    }
	
	public void addAddBankAccListener(ActionListener mal) {
        add_acc.addActionListener(mal);
    }
	public void addAddBankAccSaListener(ActionListener mal) {
        add_acc1.addActionListener(mal);
    }
	
	public void addDeleteBankAccListener(ActionListener mal) {
        delete_acc.addActionListener(mal);
    }
	
	public void addEditBankAccListener(ActionListener mal) {
        edit_acc.addActionListener(mal);
    }
	
	public void addFindBankAccListener(ActionListener mal) {
        find_acc.addActionListener(mal);
	}
	
	public void addAddMoneyListener(ActionListener mal) {
        add_money.addActionListener(mal);
	}
	
	public void addWithdrawMoneyListener(ActionListener mal) {
        sub_money.addActionListener(mal);
	}
}

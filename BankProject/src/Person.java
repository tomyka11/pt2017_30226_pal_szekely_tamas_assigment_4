
import java.io.Serializable;
import java.util.*;
public class Person implements Serializable {

	private String nume;
	private int age;
	private int id;
	public int balance;
	
	public Person(){
		super();
	}
	
	public Person(String name,int age,int id){
		this.nume=name;
		this.age=age;
		this.id=id;
		//this.balance=0;
	}	
	public String getNume(){
		return this.nume;
	}
	
	public void setNume(String name){
		this.nume=name;
	}
	
	public int getAge(){
		return this.age;
	}
	
	public void setAge(int age){
		this.age=age;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setId(int id){
		this.id=id;
	}
	
	public String toString(){
		return "Customer:"+ " "+this.nume +"has id: "+this.id;
	}
	public int getBalance(){
		return this.balance;
	}
	
	
	
	
	
	
}

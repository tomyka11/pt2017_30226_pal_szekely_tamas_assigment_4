
public class SavingAccounts extends Account{
	private static final int x=999;
	private  double ANNUAL_INTEREST_RATE;
	
	public SavingAccounts(double p){
		super();
		this.ANNUAL_INTEREST_RATE=p;
	}
	
	public int deposit(int amount){
		int i=1;
		if (amount>x){
		this.balance+=amount;
		i=0;
		}
		else System.out.println("No enough money to deposit ");
		return i;
	}
	
	public int withdraw(int amount){
		if (amount>x)
		if (amount>this.balance){
			System.out.println("Insufficient funds.");
			return 1;
		}
		else{
			this.balance-=amount;
			return 0;
		}
		else{
			System.out.println("It s not possible");
		return 1;
		}
	}
	
	public void addMountlyInterest(){
		this.balance=this.balance+(int)(ANNUAL_INTEREST_RATE/12 ) * this.balance;
	}
	
	public void setAnnualInterestRate(double newRate){
		this.ANNUAL_INTEREST_RATE=newRate;
	}

}

import java.util.HashMap;
import java.io.Serializable;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Map;
import java.util.Iterator;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Set;
import java.util.ArrayList;
import java.lang.Iterable;

public class Bank implements BankProc,Serializable{
	 
	public HashMap<Person, ArrayList<Account>> hmap = new HashMap<Person, ArrayList<Account>>();
	
	public Bank(){
		super();
	}
	
	public void addPerson(Person p){
		assert p!=null :"Precondition : p!=null";
		
		boolean st=false;
		boolean hj=false;
		for(Map.Entry<Person,ArrayList<Account>> entry : hmap.entrySet()){
			if (entry.getKey().getNume().equals(p.getNume())) st=true;
			if (entry.getKey().getId()-(p.getId())==0) hj=true;
		}
		if (st==false && hj==false){
		ArrayList<Account> a=new ArrayList<Account>();
		hmap.put(p, a);
		}
		else System.out.println("It s already exist");
		
		assert hmap.containsKey(p):"Contains";
	}
	
	public void deletePerson(Person p) {
		assert p!=null :"Precondition : p!=null";
		
		Iterator<Map.Entry<Person, ArrayList<Account>>> iterator = hmap.entrySet().iterator();
		while (iterator.hasNext()) {
		     Map.Entry<Person, ArrayList<Account>> entry = iterator.next();
		     if(entry.getKey().getNume().equals(p.getNume())){
		          iterator.remove();
		     }
		}
		
		assert !hmap.containsKey(p):"Contains";
	}
	
	public void deleteAccount(Account a) {
		assert a!=null :"Precondition : p!=null";
		
		ArrayList<Account> aa1=hmap.get(a.getPerson());
		Person ps=a.getPerson();
		//aa1.remove(a);
		int bool=0;
		Iterator<Account> it = aa1.iterator();
		Iterator<Account> it1= aa1.iterator();
		while (it1.hasNext()) {
			  Account account = it1.next();
			  if (account.getName().equals(a.getName())) {
			    bool=1;
			  }
			}
		while (it.hasNext()) {
		  Account account = it.next();
		  if (account.getId()-a.getId()==0) {
		    it.remove();
		  }
		}
		
		if (bool==1)
		a.getPerson().balance-=a.balance;
		hmap.put(ps, aa1);
		
		assert hmap.containsValue(a):"Contains";
	}
	
	public void addAccount(Account a){
		assert a!=null :"Precondition : a!=null";
		try{
		int bool=0;
		ArrayList<Account> aa=hmap.get(a.getPerson());
		Iterator<Account> it1= aa.iterator();
		while (it1.hasNext()) {
			  Account account = it1.next();
			  if (account.getId()-a.getId()==0) {
			    bool=1;
			  }
			}
		if (bool==0){
		aa.add(a);
		a.getPerson().balance+=a.balance;
		hmap.put(a.getPerson(), aa);
		}
		else System.out.println("It s already exist");
		}
		catch(ConcurrentModificationException e){
			System.out.println("Ok");
		}
		
		assert hmap.containsValue(a):"Contains";
		
	}
	
	public void report(int p){
		
	}
	
	public void editPerson(Person p){
		assert p!=null :"Precondition : p!=null";
		try{
		for(Map.Entry<Person,ArrayList<Account>> entry : hmap.entrySet()){
			if (p.getId()-entry.getKey().getId()==0){
			Person pers=entry.getKey();
			ArrayList<Account> acc=entry.getValue();
			ArrayList<Account> accuj=acc;
			hmap.remove(pers);
			for(Account a:acc)
			{
				a.setNume(p.getNume());
				p.balance+=a.getBalance();
			}
			hmap.put(p, accuj);
			
			}
		}
		}
		catch(ConcurrentModificationException e)
		{
			System.out.println("Ok");
		}
		assert hmap.containsKey(p):"Contains";
		
	}
	public void editAccount(Account a){
		assert a!=null :"Precondition : a!=null";
		ArrayList<Account> aa=hmap.get(a.getPerson());
		for(Account alap:aa){
			if (alap.getId()-a.getId()==0){
				a.getPerson().balance-=alap.balance;
				aa.remove(alap);
				aa.add(a);
				a.getPerson().balance+=a.balance;
				hmap.put(a.getPerson(), aa);
				
			}
			
		}
		
		assert hmap.containsValue(a):"contains";
		
		
	}
	
	public void saveObjects() {
		try {
			FileOutputStream fileOut = new FileOutputStream("objects.ser");
			ObjectOutputStream output = new ObjectOutputStream(fileOut);
			output.writeObject(hmap);
			output.close();
			fileOut.close();
			//System.out.println("Serialized data saved");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}@SuppressWarnings({ "unchecked", "resource" })
	public void loadObjects() {
		try {
			FileInputStream fileIn = new FileInputStream("objects.ser");
			ObjectInputStream input = new ObjectInputStream(fileIn);
			hmap = (HashMap<Person, ArrayList<Account>>) input.readObject();
				} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}
	

}

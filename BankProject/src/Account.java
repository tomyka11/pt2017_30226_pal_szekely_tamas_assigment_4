import java.io.Serializable;

public class Account implements Serializable {
	protected int balance;
	private String accperson;
	private int idaccount;
	public Person p;
	
	public Account(){
		super();
	}
	
	public Account(String nume,Person p){
		this.balance=0;
		this.accperson=nume;
		this.p=p;
		
	}
	
	public Account(int balance,String nume,int ida,Person p){
		this.balance=balance;
		this.accperson=nume;
		this.idaccount=ida;
		this.p=p;
	}
	
	public int getBalance(){
		return this.balance;
	}
	
	public int deposit(int amount){
		this.balance+=amount;
		return 0;
	}
	
	public int withdraw(int amount){
		if (amount>this.balance){
			System.out.println("Insufficient funds.");
			return 1;
		}
		else{
			this.balance-=amount;
			return 0;
		}
	}
	
	public int hashCode(){
		return this.idaccount;
	}
	
	public void setID(int id){
		this.idaccount=id;
	}
	
	public void setNume(String nume){
		this.accperson=nume;
	}
	
	public void setBalance(int balance){
		this.balance=balance;
	}
	
	public String getName(){
		return this.accperson;
	}
	
	public int getId(){
		return this.idaccount;
	}
	public Person getPerson(){
		return this.p;
	}
	

}

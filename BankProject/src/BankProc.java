
public interface BankProc {
	/**
	 *@pre p!=null
	 * @param p
	 * @post hmap.keySet.contains(p)
	 */
	public void addPerson(Person p);
	/**
	 * @pre p!=null
	 * @param p
	 * @post hmap.keySet.contains(p)
	 */
	public void deletePerson(Person p);
	/**
	 * @pre a!=null
	 * @param a
	 * @post hmap.valueSet.contains(a)
	 */
	public void deleteAccount(Account a);
	/**
	 * 
	 * @param k
	 * @param p
	 * @pre p!=null
	 * @post hmap.keySet.contains(p)
	 */
	public void editPerson(Person p);
	/**
	 * 
	 * @param a
	 * @pre a!=null
	 * @hmap.valusSet.contains(a)
	 */
	public void editAccount(Account a);
	/**
	 * 
	 * @param a
	 * @pre a!=null
	 * @post hmapacc.contains(a)
	 */
	public void addAccount(Account a);
	
    public void report(int p);
    //public void readAccount(Account a);
    //public void writeAccount(Account a);
    
}

import java.awt.event.*;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.table.DefaultTableModel;
import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JTable;
import java.util.ConcurrentModificationException;
public class Controller {
	
	private View v;
	private Bank b=new Bank();
	
	public Controller(View v){
		this.v=v;
		b.loadObjects();
		
		v.addAddBankPersListener(new AddBankPersListener());
		v.addDeleteBankPersListener(new DeleteBankPersListener());
		v.addEditBankPersListener(new EditBankPersListener());
		v.addFindBankPersListener(new FindBankPersListener());
		v.addAddBankAccListener(new AddBankAccListener());
		v.addAddBankAccSaListener(new AddBankAccSaListener());
		v.addDeleteBankAccListener(new DeleteBankAccListener());
		v.addEditBankAccListener(new EditBankAccListener());
		v.addFindBankAccListener(new FindBankAccListener());
		v.addAddMoneyListener(new AddMoneyListener());
		v.addWithdrawMoneyListener(new WithdrawMoneyListener());
		
	}
	
	class AddMoneyListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			int money=Integer.parseInt(v.getmoney());
			int id=Integer.parseInt(v.getAccId2());
			for(Map.Entry<Person,ArrayList<Account>> entry : b.hmap.entrySet()){
				ArrayList<Account> aa=entry.getValue();
				for(Account a:aa){
					if (a.getId()-id==0){
						int jk=a.deposit(money);
						if (jk==0)
						a.p.balance+=money;
						
					}
				}
			
			
		}
		}
	}
	class WithdrawMoneyListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			int money=Integer.parseInt(v.getmoney());
			int id=Integer.parseInt(v.getAccId2());
			for(Map.Entry<Person,ArrayList<Account>> entry : b.hmap.entrySet()){
				ArrayList<Account> aa=entry.getValue();
				for(Account a:aa){
					if (a.getId()-id==0){
						int jk=a.withdraw(money);
						if (jk==0)
						a.p.balance-=money;
						
					}
				}
			
			
		}
		}
	}
	class AddBankPersListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			
			Person p=new Person();
			p.setId(Integer.parseInt(v.getPersID()));
			p.setNume(v.getPersName());
			p.setAge(Integer.parseInt(v.getPersAge()));
			
			
			
			
			b.addPerson(p);
			
			
		}
	}
	
	
	class DeleteBankPersListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			
			Person p=new Person();
			p.setId(Integer.parseInt(v.getPersID()));
			p.setNume(v.getPersName());
			p.setAge(Integer.parseInt(v.getPersAge()));
			
			
			b.deletePerson(p);
			
			
		}
	}
	
	class EditBankPersListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			
			Person p=new Person();
			p.setId(Integer.parseInt(v.getPersID()));
			p.setNume(v.getPersName());
			p.setAge(Integer.parseInt(v.getPersAge()));
			
			b.editPerson(p);
			
			
		}
	}
	
	class AddBankAccListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			
			SpendingAccounts a=new SpendingAccounts();
			a.setID(Integer.parseInt(v.getAccID()));
			a.setNume(v.getAccName());
			a.setBalance(Integer.parseInt(v.getAccBalance()));
			for(Map.Entry<Person,ArrayList<Account>> entry : b.hmap.entrySet()){
				if (entry.getKey().getNume().equals(a.getName()))
						{
					  a.p=entry.getKey();
						}
				
			}
			
			
			b.addAccount(a);
			
			
		}
	}
	
	
	class AddBankAccSaListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			
			SavingAccounts a=new SavingAccounts(300);
			a.setID(Integer.parseInt(v.getAccID()));
			a.setNume(v.getAccName());
			a.setBalance(Integer.parseInt(v.getAccBalance()));
			for(Map.Entry<Person,ArrayList<Account>> entry : b.hmap.entrySet()){
				if (entry.getKey().getNume().equals(a.getName()))
						{
					  a.p=entry.getKey();
						}
				
			}
			
			
			b.addAccount(a);
			
			
		}
	}
	
	class DeleteBankAccListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			

			Account a=new Account();
			a.setID(Integer.parseInt(v.getAccID()));
			a.setNume(v.getAccName());
			a.setBalance(Integer.parseInt(v.getAccBalance()));
			for(Map.Entry<Person,ArrayList<Account>> entry : b.hmap.entrySet()){
				if (entry.getKey().getNume().equals(a.getName()))
						{
					  a.p=entry.getKey();
						}
				
			}
			
			b.deleteAccount(a);
			
			
			
		}
	}
	
	class EditBankAccListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
           try{
			Account a=new Account();
			a.setID(Integer.parseInt(v.getAccID()));
			a.setNume(v.getAccName());
			a.setBalance(Integer.parseInt(v.getAccBalance()));
			for(Map.Entry<Person,ArrayList<Account>> entry : b.hmap.entrySet()){
				if (entry.getKey().getNume().equals(a.getName()))
						{
					  a.p=entry.getKey();
						}
				
			}
			
			b.editAccount(a);
           }
           catch(ConcurrentModificationException eo)
           {
        	   System.out.println("Ok");
           }
			
			
		}
	}
	
	class FindBankPersListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			b.saveObjects();	
			JFrame fr=new JFrame("Report");
			fr.setSize(600,400);
			fr.setVisible(true);
			JTable jt=new JTable();
			fr.add(jt);
			
			String[] cN={"Name","Age","Id","Sum"};
			DefaultTableModel m=new DefaultTableModel();
			m.setColumnIdentifiers(cN);
			jt.setModel(m);
			
			m.addRow(cN);
			int i;
			Object[] row=new Object[4];
			for(Map.Entry<Person,ArrayList<Account>> entry : b.hmap.entrySet()){
		
			
				row[0]=entry.getKey().getNume();
				row[1]=entry.getKey().getAge();
				row[2]=entry.getKey().getId();
				row[3]=entry.getKey().getBalance();
				m.addRow(row);
			
			
			
		}
		}
	}
	
	
	class FindBankAccListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			b.saveObjects();
			JFrame fr=new JFrame("Report");
			fr.setSize(600,400);
			fr.setVisible(true);
			JTable jt=new JTable();
			fr.add(jt);
			
			String[] cN={"Balance","Name","Id"};
			DefaultTableModel m=new DefaultTableModel();
			m.setColumnIdentifiers(cN);
			jt.setModel(m);
			
			m.addRow(cN);
			int i;
			Object[] row=new Object[3];
			for(Map.Entry<Person,ArrayList<Account>> entry : b.hmap.entrySet()){
				ArrayList<Account> aa=entry.getValue();
			for(Account a : aa)
			{
				row[0]=a.getBalance();
				row[1]=a.getName();
				row[2]=a.getId();
				m.addRow(row);
			}
			}
			
		}
	}
	
	

}
